﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main_Library
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main_Library))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle25 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle26 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle27 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ToolStripContainer2 = New System.Windows.Forms.ToolStripContainer()
        Me.menuStrip = New System.Windows.Forms.MenuStrip()
        Me.viewMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolLabUser = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecordsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolManage = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolBooks = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolLibUsers = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolReports = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolAccount = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolAbout = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextBorrow = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.toolborrowrefresh = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolborrowdisp = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolborrowhide = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel()
        Me.SplitContainer5 = New System.Windows.Forms.SplitContainer()
        Me.dtgReturnRes = New System.Windows.Forms.DataGridView()
        Me.Column17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToolStripret = New System.Windows.Forms.ToolStrip()
        Me.TabItem3 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.SuperTabItem3 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.SuperTabItem4 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.SuperTabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.SuperTabItem5 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.SuperTabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel8 = New DevComponents.DotNetBar.TabControlPanel()
        Me.MainTab4 = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel13 = New DevComponents.DotNetBar.TabControlPanel()
        Me.SplitContainer6 = New System.Windows.Forms.SplitContainer()
        Me.dtpTodate = New System.Windows.Forms.DateTimePicker()
        Me.dtpFrom = New System.Windows.Forms.DateTimePicker()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboReport = New System.Windows.Forms.ComboBox()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Chart1 = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel15 = New DevComponents.DotNetBar.TabControlPanel()
        Me.SplitContainer7 = New System.Windows.Forms.SplitContainer()
        Me.BtnPreviewRpt = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Cmbreport = New System.Windows.Forms.ComboBox()
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.TabItem7 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.MainTab1Reports = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.ContextMember = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolMemberRefresh = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolMemberDisp = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolMemberHide = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextBook = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.toolbookrefresh = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolbookdisp = New System.Windows.Forms.ToolStripMenuItem()
        Me.toolbookhide = New System.Windows.Forms.ToolStripMenuItem()
        Me.miniToolStrip = New System.Windows.Forms.ToolStrip()
        Me.BottomToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.TopToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.RightToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.LeftToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.ContentPanel = New System.Windows.Forms.ToolStripContentPanel()
        Me.MainTab1 = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel()
        Me.MainTab2 = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel5 = New DevComponents.DotNetBar.TabControlPanel()
        Me.dtgDueRes = New System.Windows.Forms.DataGridView()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabItem5 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel()
        Me.splitborrow = New System.Windows.Forms.SplitContainer()
        Me.dtgBarrowRes = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel()
        Me.SplitContainer4 = New System.Windows.Forms.SplitContainer()
        Me.dtgLabRes = New System.Windows.Forms.DataGridView()
        Me.Column21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabItem4 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.MainTab1Manage = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel14 = New DevComponents.DotNetBar.TabControlPanel()
        Me.TabControl1 = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel18 = New DevComponents.DotNetBar.TabControlPanel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TabItem9 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.MainTab1About = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel6 = New DevComponents.DotNetBar.TabControlPanel()
        Me.SplitContainer8 = New System.Windows.Forms.SplitContainer()
        Me.dtgMemRes = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToolStrip6 = New System.Windows.Forms.ToolStrip()
        Me.ToolMemcount = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStrip3 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel4 = New System.Windows.Forms.ToolStripLabel()
        Me.txtsearchmember = New System.Windows.Forms.ToolStripTextBox()
        Me.MainTab1Users = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel7 = New DevComponents.DotNetBar.TabControlPanel()
        Me.MainTab3 = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel10 = New DevComponents.DotNetBar.TabControlPanel()
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer()
        Me.dtgVendor = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.vendor_contact_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToolStrip2 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripLabel()
        Me.TxtsearchVendor = New System.Windows.Forms.ToolStripTextBox()
        Me.MainTab3Vendor = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel11 = New DevComponents.DotNetBar.TabControlPanel()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.dtgAuthor = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.TxtsearchAuthor = New System.Windows.Forms.ToolStripTextBox()
        Me.MainTab3Authors = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel9 = New DevComponents.DotNetBar.TabControlPanel()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.dtgBook = New System.Windows.Forms.DataGridView()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToolStrip5 = New System.Windows.Forms.ToolStrip()
        Me.ToolBookscount = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStrip4 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel3 = New System.Windows.Forms.ToolStripLabel()
        Me.txtBookSearch = New System.Windows.Forms.ToolStripTextBox()
        Me.MainTab3Book = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.Maintab1Books = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.ToolStripContainer3 = New System.Windows.Forms.ToolStripContainer()
        Me.ToolStripContainer2.ContentPanel.SuspendLayout()
        Me.ToolStripContainer2.SuspendLayout()
        Me.menuStrip.SuspendLayout()
        Me.ContextBorrow.SuspendLayout()
        Me.TabControlPanel3.SuspendLayout()
        CType(Me.SplitContainer5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer5.Panel1.SuspendLayout()
        Me.SplitContainer5.SuspendLayout()
        CType(Me.dtgReturnRes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel8.SuspendLayout()
        CType(Me.MainTab4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MainTab4.SuspendLayout()
        Me.TabControlPanel13.SuspendLayout()
        CType(Me.SplitContainer6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer6.Panel1.SuspendLayout()
        Me.SplitContainer6.Panel2.SuspendLayout()
        Me.SplitContainer6.SuspendLayout()
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel15.SuspendLayout()
        CType(Me.SplitContainer7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer7.Panel1.SuspendLayout()
        Me.SplitContainer7.Panel2.SuspendLayout()
        Me.SplitContainer7.SuspendLayout()
        Me.ContextMember.SuspendLayout()
        Me.ContextBook.SuspendLayout()
        CType(Me.MainTab1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MainTab1.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        CType(Me.MainTab2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MainTab2.SuspendLayout()
        Me.TabControlPanel5.SuspendLayout()
        CType(Me.dtgDueRes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel2.SuspendLayout()
        CType(Me.splitborrow, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.splitborrow.Panel1.SuspendLayout()
        Me.splitborrow.SuspendLayout()
        CType(Me.dtgBarrowRes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel4.SuspendLayout()
        CType(Me.SplitContainer4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer4.Panel1.SuspendLayout()
        Me.SplitContainer4.SuspendLayout()
        CType(Me.dtgLabRes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel14.SuspendLayout()
        CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabControlPanel18.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel6.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel6.SuspendLayout()
        CType(Me.SplitContainer8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer8.Panel1.SuspendLayout()
        Me.SplitContainer8.SuspendLayout()
        CType(Me.dtgMemRes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip6.SuspendLayout()
        Me.ToolStrip3.SuspendLayout()
        Me.TabControlPanel7.SuspendLayout()
        CType(Me.MainTab3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MainTab3.SuspendLayout()
        Me.TabControlPanel10.SuspendLayout()
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        CType(Me.dtgVendor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip2.SuspendLayout()
        Me.TabControlPanel11.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        CType(Me.dtgAuthor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.TabControlPanel9.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.dtgBook, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip5.SuspendLayout()
        Me.ToolStrip4.SuspendLayout()
        Me.ToolStripContainer3.ContentPanel.SuspendLayout()
        Me.ToolStripContainer3.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripContainer2
        '
        '
        'ToolStripContainer2.ContentPanel
        '
        Me.ToolStripContainer2.ContentPanel.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ToolStripContainer2.ContentPanel.Controls.Add(Me.menuStrip)
        Me.ToolStripContainer2.ContentPanel.Size = New System.Drawing.Size(660, 0)
        Me.ToolStripContainer2.Dock = System.Windows.Forms.DockStyle.Top
        Me.ToolStripContainer2.Location = New System.Drawing.Point(0, 0)
        Me.ToolStripContainer2.Name = "ToolStripContainer2"
        Me.ToolStripContainer2.Size = New System.Drawing.Size(660, 25)
        Me.ToolStripContainer2.TabIndex = 3
        Me.ToolStripContainer2.Text = "ToolStripContainer2"
        '
        'menuStrip
        '
        Me.menuStrip.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.menuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.viewMenu, Me.RecordsToolStripMenuItem, Me.ToolAccount, Me.ToolAbout})
        Me.menuStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow
        Me.menuStrip.Location = New System.Drawing.Point(0, 0)
        Me.menuStrip.Name = "menuStrip"
        Me.menuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.menuStrip.Size = New System.Drawing.Size(660, 25)
        Me.menuStrip.TabIndex = 3
        Me.menuStrip.Text = "MenuStrip"
        '
        'viewMenu
        '
        Me.viewMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolLabUser})
        Me.viewMenu.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.viewMenu.ForeColor = System.Drawing.Color.White
        Me.viewMenu.Name = "viewMenu"
        Me.viewMenu.Size = New System.Drawing.Size(85, 21)
        Me.viewMenu.Text = "Application"
        '
        'ToolLabUser
        '
        Me.ToolLabUser.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolLabUser.ForeColor = System.Drawing.Color.White
        Me.ToolLabUser.Image = CType(resources.GetObject("ToolLabUser.Image"), System.Drawing.Image)
        Me.ToolLabUser.Name = "ToolLabUser"
        Me.ToolLabUser.Size = New System.Drawing.Size(156, 22)
        Me.ToolLabUser.Text = "Add Lab User"
        '
        'RecordsToolStripMenuItem
        '
        Me.RecordsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolManage, Me.ToolBooks, Me.ToolLibUsers, Me.ToolReports})
        Me.RecordsToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RecordsToolStripMenuItem.ForeColor = System.Drawing.Color.White
        Me.RecordsToolStripMenuItem.Name = "RecordsToolStripMenuItem"
        Me.RecordsToolStripMenuItem.Size = New System.Drawing.Size(68, 21)
        Me.RecordsToolStripMenuItem.Text = "Records"
        '
        'ToolManage
        '
        Me.ToolManage.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolManage.Checked = True
        Me.ToolManage.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ToolManage.ForeColor = System.Drawing.Color.White
        Me.ToolManage.Image = CType(resources.GetObject("ToolManage.Image"), System.Drawing.Image)
        Me.ToolManage.Name = "ToolManage"
        Me.ToolManage.Size = New System.Drawing.Size(197, 22)
        Me.ToolManage.Text = "Library Management"
        '
        'ToolBooks
        '
        Me.ToolBooks.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolBooks.CheckOnClick = True
        Me.ToolBooks.ForeColor = System.Drawing.Color.White
        Me.ToolBooks.Image = CType(resources.GetObject("ToolBooks.Image"), System.Drawing.Image)
        Me.ToolBooks.Name = "ToolBooks"
        Me.ToolBooks.Size = New System.Drawing.Size(197, 22)
        Me.ToolBooks.Text = "Books"
        '
        'ToolLibUsers
        '
        Me.ToolLibUsers.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolLibUsers.CheckOnClick = True
        Me.ToolLibUsers.ForeColor = System.Drawing.Color.White
        Me.ToolLibUsers.Image = CType(resources.GetObject("ToolLibUsers.Image"), System.Drawing.Image)
        Me.ToolLibUsers.Name = "ToolLibUsers"
        Me.ToolLibUsers.Size = New System.Drawing.Size(197, 22)
        Me.ToolLibUsers.Text = "Library Members"
        '
        'ToolReports
        '
        Me.ToolReports.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolReports.CheckOnClick = True
        Me.ToolReports.ForeColor = System.Drawing.Color.White
        Me.ToolReports.Image = CType(resources.GetObject("ToolReports.Image"), System.Drawing.Image)
        Me.ToolReports.Name = "ToolReports"
        Me.ToolReports.Size = New System.Drawing.Size(197, 22)
        Me.ToolReports.Text = "Reports"
        '
        'ToolAccount
        '
        Me.ToolAccount.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolAccount.ForeColor = System.Drawing.Color.White
        Me.ToolAccount.Name = "ToolAccount"
        Me.ToolAccount.Size = New System.Drawing.Size(66, 21)
        Me.ToolAccount.Text = "Account"
        '
        'ToolAbout
        '
        Me.ToolAbout.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolAbout.ForeColor = System.Drawing.Color.White
        Me.ToolAbout.Name = "ToolAbout"
        Me.ToolAbout.Size = New System.Drawing.Size(47, 21)
        Me.ToolAbout.Text = "Help"
        '
        'ContextBorrow
        '
        Me.ContextBorrow.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolborrowrefresh, Me.toolborrowdisp, Me.toolborrowhide})
        Me.ContextBorrow.Name = "ContextBook"
        Me.ContextBorrow.Size = New System.Drawing.Size(185, 70)
        '
        'toolborrowrefresh
        '
        Me.toolborrowrefresh.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.toolborrowrefresh.ForeColor = System.Drawing.Color.White
        Me.toolborrowrefresh.Name = "toolborrowrefresh"
        Me.toolborrowrefresh.Size = New System.Drawing.Size(184, 22)
        Me.toolborrowrefresh.Text = "Refresh"
        '
        'toolborrowdisp
        '
        Me.toolborrowdisp.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.toolborrowdisp.Enabled = False
        Me.toolborrowdisp.ForeColor = System.Drawing.Color.White
        Me.toolborrowdisp.Name = "toolborrowdisp"
        Me.toolborrowdisp.Size = New System.Drawing.Size(184, 22)
        Me.toolborrowdisp.Text = "Display Borrow Form"
        '
        'toolborrowhide
        '
        Me.toolborrowhide.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.toolborrowhide.ForeColor = System.Drawing.Color.White
        Me.toolborrowhide.Name = "toolborrowhide"
        Me.toolborrowhide.Size = New System.Drawing.Size(184, 22)
        Me.toolborrowhide.Text = "Hide Borrow Form"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.SplitContainer5)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(0, 27)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(658, 291)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.Black
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.Black
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.Style.GradientAngle = 90
        Me.TabControlPanel3.TabIndex = 3
        Me.TabControlPanel3.TabItem = Me.TabItem3
        '
        'SplitContainer5
        '
        Me.SplitContainer5.BackColor = System.Drawing.Color.Black
        Me.SplitContainer5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer5.Location = New System.Drawing.Point(1, 1)
        Me.SplitContainer5.Name = "SplitContainer5"
        '
        'SplitContainer5.Panel1
        '
        Me.SplitContainer5.Panel1.Controls.Add(Me.dtgReturnRes)
        Me.SplitContainer5.Panel1.Controls.Add(Me.ToolStripret)
        '
        'SplitContainer5.Panel2
        '
        Me.SplitContainer5.Panel2.AutoScroll = True
        Me.SplitContainer5.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SplitContainer5.Panel2MinSize = 317
        Me.SplitContainer5.Size = New System.Drawing.Size(656, 289)
        Me.SplitContainer5.SplitterDistance = 329
        Me.SplitContainer5.TabIndex = 9
        '
        'dtgReturnRes
        '
        Me.dtgReturnRes.AllowUserToAddRows = False
        Me.dtgReturnRes.AllowUserToDeleteRows = False
        Me.dtgReturnRes.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dtgReturnRes.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgReturnRes.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dtgReturnRes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgReturnRes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column17, Me.Column18, Me.Column19, Me.Column20})
        Me.dtgReturnRes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtgReturnRes.EnableHeadersVisualStyles = False
        Me.dtgReturnRes.GridColor = System.Drawing.SystemColors.ControlDarkDark
        Me.dtgReturnRes.Location = New System.Drawing.Point(0, 25)
        Me.dtgReturnRes.Name = "dtgReturnRes"
        Me.dtgReturnRes.ReadOnly = True
        Me.dtgReturnRes.RowHeadersWidth = 35
        Me.dtgReturnRes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgReturnRes.Size = New System.Drawing.Size(327, 262)
        Me.dtgReturnRes.TabIndex = 1
        '
        'Column17
        '
        Me.Column17.DataPropertyName = "return_trn_no"
        Me.Column17.HeaderText = "Return No."
        Me.Column17.Name = "Column17"
        Me.Column17.ReadOnly = True
        '
        'Column18
        '
        Me.Column18.DataPropertyName = "title"
        Me.Column18.HeaderText = "Book Title"
        Me.Column18.Name = "Column18"
        Me.Column18.ReadOnly = True
        Me.Column18.Width = 300
        '
        'Column19
        '
        Me.Column19.DataPropertyName = "date_return"
        Me.Column19.HeaderText = "Return Date"
        Me.Column19.Name = "Column19"
        Me.Column19.ReadOnly = True
        Me.Column19.Width = 120
        '
        'Column20
        '
        Me.Column20.DataPropertyName = "mem_name"
        Me.Column20.HeaderText = "Member Name"
        Me.Column20.Name = "Column20"
        Me.Column20.ReadOnly = True
        Me.Column20.Width = 200
        '
        'ToolStripret
        '
        Me.ToolStripret.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolStripret.Location = New System.Drawing.Point(0, 0)
        Me.ToolStripret.Name = "ToolStripret"
        Me.ToolStripret.Size = New System.Drawing.Size(327, 25)
        Me.ToolStripret.TabIndex = 0
        Me.ToolStripret.Text = "ToolStrip3"
        '
        'TabItem3
        '
        Me.TabItem3.AttachedControl = Me.TabControlPanel3
        Me.TabItem3.Icon = CType(resources.GetObject("TabItem3.Icon"), System.Drawing.Icon)
        Me.TabItem3.Name = "TabItem3"
        Me.TabItem3.Text = "Returned Books"
        '
        'SuperTabItem3
        '
        Me.SuperTabItem3.Name = "SuperTabItem3"
        Me.SuperTabItem3.Text = "Adding Lab Users"
        '
        'SuperTabItem4
        '
        Me.SuperTabItem4.Name = "SuperTabItem4"
        Me.SuperTabItem4.Text = "Members Record"
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Books Record"
        '
        'SuperTabItem5
        '
        Me.SuperTabItem5.Name = "SuperTabItem5"
        Me.SuperTabItem5.Text = "Reports"
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "Library Management"
        '
        'TabControlPanel8
        '
        Me.TabControlPanel8.Controls.Add(Me.MainTab4)
        Me.TabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel8.Location = New System.Drawing.Point(0, 0)
        Me.TabControlPanel8.Name = "TabControlPanel8"
        Me.TabControlPanel8.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel8.Size = New System.Drawing.Size(660, 320)
        Me.TabControlPanel8.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel8.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel8.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel8.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Top), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel8.Style.GradientAngle = -90
        Me.TabControlPanel8.TabIndex = 4
        Me.TabControlPanel8.TabItem = Me.MainTab1Reports
        '
        'MainTab4
        '
        Me.MainTab4.BackColor = System.Drawing.Color.White
        Me.MainTab4.CanReorderTabs = True
        Me.MainTab4.CloseButtonOnTabsAlwaysDisplayed = False
        Me.MainTab4.CloseButtonPosition = DevComponents.DotNetBar.eTabCloseButtonPosition.Right
        Me.MainTab4.ColorScheme.TabBackground = System.Drawing.SystemColors.ControlDarkDark
        Me.MainTab4.ColorScheme.TabBackground2 = System.Drawing.Color.Empty
        Me.MainTab4.ColorScheme.TabBorder = System.Drawing.Color.Black
        Me.MainTab4.ColorScheme.TabItemBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(215, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(249, Byte), Integer)), 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(248, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(179, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(245, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(247, Byte), Integer)), 1.0!)})
        Me.MainTab4.ColorScheme.TabItemHotBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(235, Byte), Integer)), 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(168, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(89, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(141, Byte), Integer)), 1.0!)})
        Me.MainTab4.ColorScheme.TabItemHotText = System.Drawing.Color.White
        Me.MainTab4.ColorScheme.TabItemSelectedBackground = System.Drawing.SystemColors.Highlight
        Me.MainTab4.ColorScheme.TabItemSelectedBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.White, 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 1.0!)})
        Me.MainTab4.ColorScheme.TabItemSelectedBorder = System.Drawing.Color.Empty
        Me.MainTab4.ColorScheme.TabItemSelectedBorderLight = System.Drawing.Color.Empty
        Me.MainTab4.ColorScheme.TabItemSelectedText = System.Drawing.Color.White
        Me.MainTab4.ColorScheme.TabItemText = System.Drawing.Color.White
        Me.MainTab4.ColorScheme.TabPanelBackground = System.Drawing.Color.White
        Me.MainTab4.ColorScheme.TabPanelBackground2 = System.Drawing.Color.Empty
        Me.MainTab4.ColorScheme.TabPanelBorder = System.Drawing.Color.Black
        Me.MainTab4.Controls.Add(Me.TabControlPanel13)
        Me.MainTab4.Controls.Add(Me.TabControlPanel15)
        Me.MainTab4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainTab4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainTab4.ForeColor = System.Drawing.Color.White
        Me.MainTab4.Location = New System.Drawing.Point(1, 1)
        Me.MainTab4.Name = "MainTab4"
        Me.MainTab4.SelectedTabFont = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainTab4.SelectedTabIndex = 0
        Me.MainTab4.Size = New System.Drawing.Size(658, 318)
        Me.MainTab4.Style = DevComponents.DotNetBar.eTabStripStyle.Flat
        Me.MainTab4.TabIndex = 4
        Me.MainTab4.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.MainTab4.Tabs.Add(Me.TabItem1)
        Me.MainTab4.Tabs.Add(Me.TabItem7)
        Me.MainTab4.Text = "TabControl2"
        '
        'TabControlPanel13
        '
        Me.TabControlPanel13.Controls.Add(Me.SplitContainer6)
        Me.TabControlPanel13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel13.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControlPanel13.Location = New System.Drawing.Point(0, 27)
        Me.TabControlPanel13.Name = "TabControlPanel13"
        Me.TabControlPanel13.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel13.Size = New System.Drawing.Size(658, 291)
        Me.TabControlPanel13.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel13.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel13.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel13.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel13.Style.GradientAngle = 90
        Me.TabControlPanel13.TabIndex = 1
        Me.TabControlPanel13.TabItem = Me.TabItem1
        '
        'SplitContainer6
        '
        Me.SplitContainer6.BackColor = System.Drawing.Color.Black
        Me.SplitContainer6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer6.Location = New System.Drawing.Point(1, 1)
        Me.SplitContainer6.Name = "SplitContainer6"
        Me.SplitContainer6.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer6.Panel1
        '
        Me.SplitContainer6.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SplitContainer6.Panel1.Controls.Add(Me.dtpTodate)
        Me.SplitContainer6.Panel1.Controls.Add(Me.dtpFrom)
        Me.SplitContainer6.Panel1.Controls.Add(Me.btnPrint)
        Me.SplitContainer6.Panel1.Controls.Add(Me.btnPreview)
        Me.SplitContainer6.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer6.Panel1.Controls.Add(Me.cboReport)
        Me.SplitContainer6.Panel1.Controls.Add(Me.btnLoad)
        Me.SplitContainer6.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer6.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer6.Panel1MinSize = 10
        '
        'SplitContainer6.Panel2
        '
        Me.SplitContainer6.Panel2.AutoScroll = True
        Me.SplitContainer6.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SplitContainer6.Panel2.Controls.Add(Me.Chart1)
        Me.SplitContainer6.Panel2MinSize = 223
        Me.SplitContainer6.Size = New System.Drawing.Size(656, 289)
        Me.SplitContainer6.SplitterDistance = 62
        Me.SplitContainer6.TabIndex = 11
        '
        'dtpTodate
        '
        Me.dtpTodate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTodate.Location = New System.Drawing.Point(308, 46)
        Me.dtpTodate.Name = "dtpTodate"
        Me.dtpTodate.Size = New System.Drawing.Size(108, 23)
        Me.dtpTodate.TabIndex = 18
        '
        'dtpFrom
        '
        Me.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFrom.Location = New System.Drawing.Point(162, 46)
        Me.dtpFrom.Name = "dtpFrom"
        Me.dtpFrom.Size = New System.Drawing.Size(113, 23)
        Me.dtpFrom.TabIndex = 17
        '
        'btnPrint
        '
        Me.btnPrint.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.btnPrint.FlatAppearance.BorderSize = 2
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Image = CType(resources.GetObject("btnPrint.Image"), System.Drawing.Image)
        Me.btnPrint.Location = New System.Drawing.Point(555, 12)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(56, 57)
        Me.btnPrint.TabIndex = 16
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.btnPreview.FlatAppearance.BorderSize = 2
        Me.btnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPreview.Image = CType(resources.GetObject("btnPreview.Image"), System.Drawing.Image)
        Me.btnPreview.Location = New System.Drawing.Point(493, 12)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(56, 55)
        Me.btnPreview.TabIndex = 15
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(27, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 15)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "Report Name"
        '
        'cboReport
        '
        Me.cboReport.BackColor = System.Drawing.Color.Black
        Me.cboReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cboReport.ForeColor = System.Drawing.Color.White
        Me.cboReport.FormattingEnabled = True
        Me.cboReport.Items.AddRange(New Object() {"Lab Users by Department"})
        Me.cboReport.Location = New System.Drawing.Point(121, 15)
        Me.cboReport.Name = "cboReport"
        Me.cboReport.Size = New System.Drawing.Size(295, 23)
        Me.cboReport.TabIndex = 13
        '
        'btnLoad
        '
        Me.btnLoad.FlatAppearance.BorderColor = System.Drawing.SystemColors.ControlDarkDark
        Me.btnLoad.FlatAppearance.BorderSize = 2
        Me.btnLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLoad.Image = CType(resources.GetObject("btnLoad.Image"), System.Drawing.Image)
        Me.btnLoad.Location = New System.Drawing.Point(431, 12)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(56, 55)
        Me.btnLoad.TabIndex = 12
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(279, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(21, 15)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "To"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(118, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 15)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "From"
        '
        'Chart1
        '
        Me.Chart1.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        ChartArea1.Name = "ChartArea1"
        Me.Chart1.ChartAreas.Add(ChartArea1)
        Legend1.Name = "Legend1"
        Me.Chart1.Legends.Add(Legend1)
        Me.Chart1.Location = New System.Drawing.Point(103, 7)
        Me.Chart1.Name = "Chart1"
        Me.Chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel
        Series1.ChartArea = "ChartArea1"
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Me.Chart1.Series.Add(Series1)
        Me.Chart1.Size = New System.Drawing.Size(459, 203)
        Me.Chart1.TabIndex = 0
        Me.Chart1.Text = "Chart1"
        '
        'TabItem1
        '
        Me.TabItem1.AttachedControl = Me.TabControlPanel13
        Me.TabItem1.Icon = CType(resources.GetObject("TabItem1.Icon"), System.Drawing.Icon)
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "Chart Viewer - Lab User"
        '
        'TabControlPanel15
        '
        Me.TabControlPanel15.Controls.Add(Me.SplitContainer7)
        Me.TabControlPanel15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel15.Location = New System.Drawing.Point(0, 27)
        Me.TabControlPanel15.Name = "TabControlPanel15"
        Me.TabControlPanel15.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel15.Size = New System.Drawing.Size(658, 291)
        Me.TabControlPanel15.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel15.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel15.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel15.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel15.Style.GradientAngle = 90
        Me.TabControlPanel15.TabIndex = 3
        Me.TabControlPanel15.TabItem = Me.TabItem7
        '
        'SplitContainer7
        '
        Me.SplitContainer7.BackColor = System.Drawing.Color.Black
        Me.SplitContainer7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer7.Location = New System.Drawing.Point(1, 1)
        Me.SplitContainer7.Name = "SplitContainer7"
        Me.SplitContainer7.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer7.Panel1
        '
        Me.SplitContainer7.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SplitContainer7.Panel1.Controls.Add(Me.BtnPreviewRpt)
        Me.SplitContainer7.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer7.Panel1.Controls.Add(Me.Cmbreport)
        Me.SplitContainer7.Panel1MinSize = 10
        '
        'SplitContainer7.Panel2
        '
        Me.SplitContainer7.Panel2.AutoScroll = True
        Me.SplitContainer7.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SplitContainer7.Panel2.Controls.Add(Me.CrystalReportViewer1)
        Me.SplitContainer7.Panel2MinSize = 223
        Me.SplitContainer7.Size = New System.Drawing.Size(656, 289)
        Me.SplitContainer7.SplitterDistance = 62
        Me.SplitContainer7.TabIndex = 12
        '
        'BtnPreviewRpt
        '
        Me.BtnPreviewRpt.FlatAppearance.BorderSize = 2
        Me.BtnPreviewRpt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnPreviewRpt.Image = CType(resources.GetObject("BtnPreviewRpt.Image"), System.Drawing.Image)
        Me.BtnPreviewRpt.Location = New System.Drawing.Point(491, 16)
        Me.BtnPreviewRpt.Name = "BtnPreviewRpt"
        Me.BtnPreviewRpt.Size = New System.Drawing.Size(56, 48)
        Me.BtnPreviewRpt.TabIndex = 20
        Me.BtnPreviewRpt.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(79, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 17)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Report Name"
        '
        'Cmbreport
        '
        Me.Cmbreport.BackColor = System.Drawing.Color.Black
        Me.Cmbreport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Cmbreport.ForeColor = System.Drawing.Color.White
        Me.Cmbreport.FormattingEnabled = True
        Me.Cmbreport.Items.AddRange(New Object() {"List of Books", "List of Borrowed Books", "List of Books Returned"})
        Me.Cmbreport.Location = New System.Drawing.Point(173, 29)
        Me.Cmbreport.Name = "Cmbreport"
        Me.Cmbreport.Size = New System.Drawing.Size(295, 25)
        Me.Cmbreport.TabIndex = 18
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.ShowGroupTreeButton = False
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(654, 221)
        Me.CrystalReportViewer1.TabIndex = 0
        Me.CrystalReportViewer1.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'TabItem7
        '
        Me.TabItem7.AttachedControl = Me.TabControlPanel15
        Me.TabItem7.Icon = CType(resources.GetObject("TabItem7.Icon"), System.Drawing.Icon)
        Me.TabItem7.Name = "TabItem7"
        Me.TabItem7.Text = "Report Viewer - Books"
        '
        'MainTab1Reports
        '
        Me.MainTab1Reports.AttachedControl = Me.TabControlPanel8
        Me.MainTab1Reports.Icon = CType(resources.GetObject("MainTab1Reports.Icon"), System.Drawing.Icon)
        Me.MainTab1Reports.Name = "MainTab1Reports"
        Me.MainTab1Reports.Text = "Reports"
        Me.MainTab1Reports.Visible = False
        '
        'ContextMember
        '
        Me.ContextMember.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolMemberRefresh, Me.ToolMemberDisp, Me.ToolMemberHide})
        Me.ContextMember.Name = "ContextBook"
        Me.ContextMember.Size = New System.Drawing.Size(197, 70)
        '
        'ToolMemberRefresh
        '
        Me.ToolMemberRefresh.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolMemberRefresh.ForeColor = System.Drawing.Color.White
        Me.ToolMemberRefresh.Name = "ToolMemberRefresh"
        Me.ToolMemberRefresh.Size = New System.Drawing.Size(196, 22)
        Me.ToolMemberRefresh.Text = "Refresh"
        '
        'ToolMemberDisp
        '
        Me.ToolMemberDisp.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolMemberDisp.Enabled = False
        Me.ToolMemberDisp.ForeColor = System.Drawing.Color.White
        Me.ToolMemberDisp.Name = "ToolMemberDisp"
        Me.ToolMemberDisp.Size = New System.Drawing.Size(196, 22)
        Me.ToolMemberDisp.Text = "Display Members Form"
        '
        'ToolMemberHide
        '
        Me.ToolMemberHide.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolMemberHide.ForeColor = System.Drawing.Color.White
        Me.ToolMemberHide.Name = "ToolMemberHide"
        Me.ToolMemberHide.Size = New System.Drawing.Size(196, 22)
        Me.ToolMemberHide.Text = "Hide Members Form"
        '
        'ContextBook
        '
        Me.ContextBook.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolbookrefresh, Me.toolbookdisp, Me.toolbookhide})
        Me.ContextBook.Name = "ContextBook"
        Me.ContextBook.Size = New System.Drawing.Size(174, 70)
        '
        'toolbookrefresh
        '
        Me.toolbookrefresh.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.toolbookrefresh.ForeColor = System.Drawing.Color.White
        Me.toolbookrefresh.Name = "toolbookrefresh"
        Me.toolbookrefresh.Size = New System.Drawing.Size(173, 22)
        Me.toolbookrefresh.Text = "Refresh"
        '
        'toolbookdisp
        '
        Me.toolbookdisp.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.toolbookdisp.Enabled = False
        Me.toolbookdisp.ForeColor = System.Drawing.Color.White
        Me.toolbookdisp.Name = "toolbookdisp"
        Me.toolbookdisp.Size = New System.Drawing.Size(173, 22)
        Me.toolbookdisp.Text = "Display Book Form"
        '
        'toolbookhide
        '
        Me.toolbookhide.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.toolbookhide.ForeColor = System.Drawing.Color.White
        Me.toolbookhide.Name = "toolbookhide"
        Me.toolbookhide.Size = New System.Drawing.Size(173, 22)
        Me.toolbookhide.Text = "Hide Book Form"
        '
        'miniToolStrip
        '
        Me.miniToolStrip.AutoSize = False
        Me.miniToolStrip.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.miniToolStrip.CanOverflow = False
        Me.miniToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me.miniToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.miniToolStrip.Location = New System.Drawing.Point(296, 3)
        Me.miniToolStrip.Name = "miniToolStrip"
        Me.miniToolStrip.Size = New System.Drawing.Size(333, 25)
        Me.miniToolStrip.TabIndex = 7
        '
        'BottomToolStripPanel
        '
        Me.BottomToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.BottomToolStripPanel.Name = "BottomToolStripPanel"
        Me.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.BottomToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.BottomToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'TopToolStripPanel
        '
        Me.TopToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopToolStripPanel.Name = "TopToolStripPanel"
        Me.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.TopToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.TopToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'RightToolStripPanel
        '
        Me.RightToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.RightToolStripPanel.Name = "RightToolStripPanel"
        Me.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.RightToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.RightToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'LeftToolStripPanel
        '
        Me.LeftToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.LeftToolStripPanel.Name = "LeftToolStripPanel"
        Me.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.LeftToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.LeftToolStripPanel.Size = New System.Drawing.Size(0, 0)
        '
        'ContentPanel
        '
        Me.ContentPanel.Size = New System.Drawing.Size(660, 372)
        '
        'MainTab1
        '
        Me.MainTab1.BackColor = System.Drawing.Color.White
        Me.MainTab1.CanReorderTabs = True
        Me.MainTab1.ColorScheme.TabBackground = System.Drawing.SystemColors.ControlDarkDark
        Me.MainTab1.ColorScheme.TabBackground2 = System.Drawing.Color.Empty
        Me.MainTab1.ColorScheme.TabBorder = System.Drawing.Color.Black
        Me.MainTab1.ColorScheme.TabItemBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(215, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(249, Byte), Integer)), 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(248, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(179, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(245, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(247, Byte), Integer)), 1.0!)})
        Me.MainTab1.ColorScheme.TabItemHotBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(235, Byte), Integer)), 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(168, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(89, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(141, Byte), Integer)), 1.0!)})
        Me.MainTab1.ColorScheme.TabItemHotText = System.Drawing.Color.White
        Me.MainTab1.ColorScheme.TabItemSelectedBackground = System.Drawing.SystemColors.Highlight
        Me.MainTab1.ColorScheme.TabItemSelectedBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.White, 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 1.0!)})
        Me.MainTab1.ColorScheme.TabItemSelectedBorder = System.Drawing.Color.Empty
        Me.MainTab1.ColorScheme.TabItemSelectedBorderDark = System.Drawing.Color.Empty
        Me.MainTab1.ColorScheme.TabItemSelectedBorderLight = System.Drawing.Color.Empty
        Me.MainTab1.ColorScheme.TabItemSelectedText = System.Drawing.Color.White
        Me.MainTab1.ColorScheme.TabItemText = System.Drawing.Color.White
        Me.MainTab1.ColorScheme.TabPanelBackground = System.Drawing.Color.White
        Me.MainTab1.ColorScheme.TabPanelBackground2 = System.Drawing.Color.Empty
        Me.MainTab1.ColorScheme.TabPanelBorder = System.Drawing.Color.Black
        Me.MainTab1.Controls.Add(Me.TabControlPanel14)
        Me.MainTab1.Controls.Add(Me.TabControlPanel1)
        Me.MainTab1.Controls.Add(Me.TabControlPanel8)
        Me.MainTab1.Controls.Add(Me.TabControlPanel6)
        Me.MainTab1.Controls.Add(Me.TabControlPanel7)
        Me.MainTab1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainTab1.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainTab1.ForeColor = System.Drawing.Color.White
        Me.MainTab1.Location = New System.Drawing.Point(0, 0)
        Me.MainTab1.Name = "MainTab1"
        Me.MainTab1.SelectedTabFont = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainTab1.SelectedTabIndex = 0
        Me.MainTab1.Size = New System.Drawing.Size(660, 347)
        Me.MainTab1.Style = DevComponents.DotNetBar.eTabStripStyle.Flat
        Me.MainTab1.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Bottom
        Me.MainTab1.TabIndex = 0
        Me.MainTab1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.MainTab1.Tabs.Add(Me.MainTab1Manage)
        Me.MainTab1.Tabs.Add(Me.Maintab1Books)
        Me.MainTab1.Tabs.Add(Me.MainTab1Users)
        Me.MainTab1.Tabs.Add(Me.MainTab1Reports)
        Me.MainTab1.Tabs.Add(Me.MainTab1About)
        Me.MainTab1.Text = "TabControl1"
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.MainTab2)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(660, 320)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Top), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.Style.GradientAngle = -90
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.MainTab1Manage
        '
        'MainTab2
        '
        Me.MainTab2.BackColor = System.Drawing.Color.White
        Me.MainTab2.CanReorderTabs = True
        Me.MainTab2.CloseButtonOnTabsAlwaysDisplayed = False
        Me.MainTab2.CloseButtonPosition = DevComponents.DotNetBar.eTabCloseButtonPosition.Right
        Me.MainTab2.ColorScheme.TabBackground = System.Drawing.SystemColors.ControlDarkDark
        Me.MainTab2.ColorScheme.TabBackground2 = System.Drawing.Color.Empty
        Me.MainTab2.ColorScheme.TabBorder = System.Drawing.Color.Black
        Me.MainTab2.ColorScheme.TabItemHotBackground = System.Drawing.Color.Empty
        Me.MainTab2.ColorScheme.TabItemHotText = System.Drawing.Color.Empty
        Me.MainTab2.ColorScheme.TabItemSelectedBackground = System.Drawing.SystemColors.Highlight
        Me.MainTab2.ColorScheme.TabItemSelectedBorder = System.Drawing.Color.Empty
        Me.MainTab2.ColorScheme.TabItemSelectedBorderLight = System.Drawing.Color.Empty
        Me.MainTab2.ColorScheme.TabItemSelectedText = System.Drawing.Color.White
        Me.MainTab2.ColorScheme.TabItemText = System.Drawing.Color.White
        Me.MainTab2.ColorScheme.TabPanelBackground = System.Drawing.Color.Black
        Me.MainTab2.ColorScheme.TabPanelBackground2 = System.Drawing.Color.Black
        Me.MainTab2.ColorScheme.TabPanelBorder = System.Drawing.Color.Black
        Me.MainTab2.Controls.Add(Me.TabControlPanel2)
        Me.MainTab2.Controls.Add(Me.TabControlPanel3)
        Me.MainTab2.Controls.Add(Me.TabControlPanel5)
        Me.MainTab2.Controls.Add(Me.TabControlPanel4)
        Me.MainTab2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainTab2.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainTab2.ForeColor = System.Drawing.Color.White
        Me.MainTab2.Location = New System.Drawing.Point(1, 1)
        Me.MainTab2.Name = "MainTab2"
        Me.MainTab2.SelectedTabFont = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainTab2.SelectedTabIndex = 0
        Me.MainTab2.Size = New System.Drawing.Size(658, 318)
        Me.MainTab2.Style = DevComponents.DotNetBar.eTabStripStyle.Flat
        Me.MainTab2.TabIndex = 2
        Me.MainTab2.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.MainTab2.Tabs.Add(Me.TabItem2)
        Me.MainTab2.Tabs.Add(Me.TabItem5)
        Me.MainTab2.Tabs.Add(Me.TabItem3)
        Me.MainTab2.Tabs.Add(Me.TabItem4)
        Me.MainTab2.Text = "TabControl2"
        '
        'TabControlPanel5
        '
        Me.TabControlPanel5.Controls.Add(Me.dtgDueRes)
        Me.TabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel5.Location = New System.Drawing.Point(0, 27)
        Me.TabControlPanel5.Name = "TabControlPanel5"
        Me.TabControlPanel5.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel5.Size = New System.Drawing.Size(658, 291)
        Me.TabControlPanel5.Style.BackColor1.Color = System.Drawing.Color.Black
        Me.TabControlPanel5.Style.BackColor2.Color = System.Drawing.Color.Black
        Me.TabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel5.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel5.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel5.Style.GradientAngle = 90
        Me.TabControlPanel5.TabIndex = 2
        Me.TabControlPanel5.TabItem = Me.TabItem5
        '
        'dtgDueRes
        '
        Me.dtgDueRes.AllowUserToAddRows = False
        Me.dtgDueRes.AllowUserToDeleteRows = False
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White
        Me.dtgDueRes.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle6
        Me.dtgDueRes.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dtgDueRes.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.ControlLight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgDueRes.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dtgDueRes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dtgDueRes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column11, Me.Column12, Me.Column13, Me.Column14, Me.Column15, Me.Column16})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgDueRes.DefaultCellStyle = DataGridViewCellStyle8
        Me.dtgDueRes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtgDueRes.EnableHeadersVisualStyles = False
        Me.dtgDueRes.GridColor = System.Drawing.SystemColors.ControlDarkDark
        Me.dtgDueRes.Location = New System.Drawing.Point(1, 1)
        Me.dtgDueRes.Name = "dtgDueRes"
        Me.dtgDueRes.ReadOnly = True
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgDueRes.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dtgDueRes.RowHeadersWidth = 35
        Me.dtgDueRes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgDueRes.Size = New System.Drawing.Size(656, 289)
        Me.dtgDueRes.TabIndex = 3
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "title"
        Me.Column11.HeaderText = "Book Title"
        Me.Column11.Name = "Column11"
        Me.Column11.ReadOnly = True
        Me.Column11.Width = 300
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "mem_name"
        Me.Column12.HeaderText = "Barrower Name"
        Me.Column12.Name = "Column12"
        Me.Column12.ReadOnly = True
        Me.Column12.Width = 200
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "date_barrow"
        Me.Column13.HeaderText = "Barrow Date"
        Me.Column13.Name = "Column13"
        Me.Column13.ReadOnly = True
        Me.Column13.Width = 120
        '
        'Column14
        '
        Me.Column14.DataPropertyName = "due_date"
        Me.Column14.HeaderText = "Due Date"
        Me.Column14.Name = "Column14"
        Me.Column14.ReadOnly = True
        '
        'Column15
        '
        Me.Column15.DataPropertyName = "contact_no"
        Me.Column15.HeaderText = "Contact #"
        Me.Column15.Name = "Column15"
        Me.Column15.ReadOnly = True
        '
        'Column16
        '
        Me.Column16.DataPropertyName = "address"
        Me.Column16.HeaderText = "Address"
        Me.Column16.Name = "Column16"
        Me.Column16.ReadOnly = True
        Me.Column16.Width = 300
        '
        'TabItem5
        '
        Me.TabItem5.AttachedControl = Me.TabControlPanel5
        Me.TabItem5.Icon = CType(resources.GetObject("TabItem5.Icon"), System.Drawing.Icon)
        Me.TabItem5.Name = "TabItem5"
        Me.TabItem5.Text = "Due Books"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.splitborrow)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControlPanel2.Location = New System.Drawing.Point(0, 27)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(658, 291)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.Black
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.Black
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.Style.GradientAngle = 90
        Me.TabControlPanel2.TabIndex = 1
        Me.TabControlPanel2.TabItem = Me.TabItem2
        '
        'splitborrow
        '
        Me.splitborrow.BackColor = System.Drawing.Color.Black
        Me.splitborrow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.splitborrow.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splitborrow.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.splitborrow.Location = New System.Drawing.Point(1, 1)
        Me.splitborrow.Name = "splitborrow"
        '
        'splitborrow.Panel1
        '
        Me.splitborrow.Panel1.Controls.Add(Me.dtgBarrowRes)
        '
        'splitborrow.Panel2
        '
        Me.splitborrow.Panel2.AutoScroll = True
        Me.splitborrow.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.splitborrow.Panel2MinSize = 317
        Me.splitborrow.Size = New System.Drawing.Size(656, 289)
        Me.splitborrow.SplitterDistance = 335
        Me.splitborrow.TabIndex = 7
        '
        'dtgBarrowRes
        '
        Me.dtgBarrowRes.AllowUserToAddRows = False
        Me.dtgBarrowRes.AllowUserToDeleteRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.White
        Me.dtgBarrowRes.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dtgBarrowRes.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dtgBarrowRes.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlLight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgBarrowRes.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dtgBarrowRes.ColumnHeadersHeight = 24
        Me.dtgBarrowRes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dtgBarrowRes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.Column10})
        Me.dtgBarrowRes.ContextMenuStrip = Me.ContextBorrow
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgBarrowRes.DefaultCellStyle = DataGridViewCellStyle4
        Me.dtgBarrowRes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtgBarrowRes.EnableHeadersVisualStyles = False
        Me.dtgBarrowRes.GridColor = System.Drawing.SystemColors.ControlDarkDark
        Me.dtgBarrowRes.Location = New System.Drawing.Point(0, 0)
        Me.dtgBarrowRes.MultiSelect = False
        Me.dtgBarrowRes.Name = "dtgBarrowRes"
        Me.dtgBarrowRes.ReadOnly = True
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgBarrowRes.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dtgBarrowRes.RowHeadersWidth = 35
        Me.dtgBarrowRes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgBarrowRes.Size = New System.Drawing.Size(333, 287)
        Me.dtgBarrowRes.TabIndex = 10
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "barrow_trn_no"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Issuance No."
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 90
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "title"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Book Title"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 280
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "mem_name"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Barrower Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 120
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "date_barrow"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Date Barrow"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "due_date"
        Me.Column10.HeaderText = "Due Date"
        Me.Column10.Name = "Column10"
        Me.Column10.ReadOnly = True
        Me.Column10.Width = 80
        '
        'TabItem2
        '
        Me.TabItem2.AttachedControl = Me.TabControlPanel2
        Me.TabItem2.Icon = CType(resources.GetObject("TabItem2.Icon"), System.Drawing.Icon)
        Me.TabItem2.Name = "TabItem2"
        Me.TabItem2.Text = "Borrowed Books"
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.SplitContainer4)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(0, 27)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(658, 291)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.Black
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.Black
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.Style.GradientAngle = 90
        Me.TabControlPanel4.TabIndex = 4
        Me.TabControlPanel4.TabItem = Me.TabItem4
        '
        'SplitContainer4
        '
        Me.SplitContainer4.BackColor = System.Drawing.Color.Black
        Me.SplitContainer4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer4.Location = New System.Drawing.Point(1, 1)
        Me.SplitContainer4.Name = "SplitContainer4"
        '
        'SplitContainer4.Panel1
        '
        Me.SplitContainer4.Panel1.Controls.Add(Me.dtgLabRes)
        '
        'SplitContainer4.Panel2
        '
        Me.SplitContainer4.Panel2.AutoScroll = True
        Me.SplitContainer4.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SplitContainer4.Panel2MinSize = 317
        Me.SplitContainer4.Size = New System.Drawing.Size(656, 289)
        Me.SplitContainer4.SplitterDistance = 329
        Me.SplitContainer4.TabIndex = 8
        '
        'dtgLabRes
        '
        Me.dtgLabRes.AllowUserToAddRows = False
        Me.dtgLabRes.AllowUserToDeleteRows = False
        DataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.White
        Me.dtgLabRes.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle10
        Me.dtgLabRes.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dtgLabRes.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgLabRes.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dtgLabRes.ColumnHeadersHeight = 24
        Me.dtgLabRes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dtgLabRes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column21, Me.Column22, Me.Column24, Me.Column23})
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgLabRes.DefaultCellStyle = DataGridViewCellStyle12
        Me.dtgLabRes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtgLabRes.EnableHeadersVisualStyles = False
        Me.dtgLabRes.GridColor = System.Drawing.SystemColors.ControlDarkDark
        Me.dtgLabRes.Location = New System.Drawing.Point(0, 0)
        Me.dtgLabRes.MultiSelect = False
        Me.dtgLabRes.Name = "dtgLabRes"
        Me.dtgLabRes.ReadOnly = True
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgLabRes.RowHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dtgLabRes.RowHeadersWidth = 35
        Me.dtgLabRes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgLabRes.Size = New System.Drawing.Size(327, 287)
        Me.dtgLabRes.TabIndex = 6
        '
        'Column21
        '
        Me.Column21.DataPropertyName = "log_id"
        Me.Column21.HeaderText = "Log No."
        Me.Column21.Name = "Column21"
        Me.Column21.ReadOnly = True
        '
        'Column22
        '
        Me.Column22.DataPropertyName = "mem_name"
        Me.Column22.HeaderText = "Member Name"
        Me.Column22.Name = "Column22"
        Me.Column22.ReadOnly = True
        Me.Column22.Width = 200
        '
        'Column24
        '
        Me.Column24.DataPropertyName = "code"
        Me.Column24.HeaderText = "Course"
        Me.Column24.Name = "Column24"
        Me.Column24.ReadOnly = True
        '
        'Column23
        '
        Me.Column23.DataPropertyName = "entry_time"
        Me.Column23.HeaderText = "Entry Time"
        Me.Column23.Name = "Column23"
        Me.Column23.ReadOnly = True
        '
        'TabItem4
        '
        Me.TabItem4.AttachedControl = Me.TabControlPanel4
        Me.TabItem4.Icon = CType(resources.GetObject("TabItem4.Icon"), System.Drawing.Icon)
        Me.TabItem4.Name = "TabItem4"
        Me.TabItem4.Text = "Current Library User"
        '
        'MainTab1Manage
        '
        Me.MainTab1Manage.AttachedControl = Me.TabControlPanel1
        Me.MainTab1Manage.Icon = CType(resources.GetObject("MainTab1Manage.Icon"), System.Drawing.Icon)
        Me.MainTab1Manage.Name = "MainTab1Manage"
        Me.MainTab1Manage.Text = "Library Management"
        '
        'TabControlPanel14
        '
        Me.TabControlPanel14.Controls.Add(Me.TabControl1)
        Me.TabControlPanel14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel14.Location = New System.Drawing.Point(0, 0)
        Me.TabControlPanel14.Name = "TabControlPanel14"
        Me.TabControlPanel14.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel14.Size = New System.Drawing.Size(660, 320)
        Me.TabControlPanel14.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel14.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel14.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel14.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Top), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel14.Style.GradientAngle = -90
        Me.TabControlPanel14.TabIndex = 5
        Me.TabControlPanel14.TabItem = Me.MainTab1About
        '
        'TabControl1
        '
        Me.TabControl1.CanReorderTabs = True
        Me.TabControl1.ColorScheme.TabBackground = System.Drawing.SystemColors.ControlDarkDark
        Me.TabControl1.ColorScheme.TabBackground2 = System.Drawing.SystemColors.ControlDarkDark
        Me.TabControl1.ColorScheme.TabBorder = System.Drawing.Color.Black
        Me.TabControl1.ColorScheme.TabItemBackground = System.Drawing.Color.Empty
        Me.TabControl1.ColorScheme.TabItemBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(215, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(249, Byte), Integer)), 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(248, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(179, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(245, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(247, Byte), Integer)), 1.0!)})
        Me.TabControl1.ColorScheme.TabItemBorder = System.Drawing.Color.Black
        Me.TabControl1.ColorScheme.TabItemBorderDark = System.Drawing.Color.Black
        Me.TabControl1.ColorScheme.TabItemBorderLight = System.Drawing.Color.Black
        Me.TabControl1.ColorScheme.TabItemHotBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(235, Byte), Integer)), 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(168, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(89, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(141, Byte), Integer)), 1.0!)})
        Me.TabControl1.ColorScheme.TabItemHotBorder = System.Drawing.Color.Empty
        Me.TabControl1.ColorScheme.TabItemHotBorderDark = System.Drawing.Color.Empty
        Me.TabControl1.ColorScheme.TabItemHotBorderLight = System.Drawing.Color.Empty
        Me.TabControl1.ColorScheme.TabItemHotText = System.Drawing.Color.White
        Me.TabControl1.ColorScheme.TabItemSelectedBackground = System.Drawing.SystemColors.ControlDarkDark
        Me.TabControl1.ColorScheme.TabItemSelectedBackground2 = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TabControl1.ColorScheme.TabItemSelectedBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.White, 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 1.0!)})
        Me.TabControl1.ColorScheme.TabItemSelectedBorder = System.Drawing.Color.Empty
        Me.TabControl1.ColorScheme.TabItemSelectedBorderDark = System.Drawing.Color.Empty
        Me.TabControl1.ColorScheme.TabItemSelectedBorderLight = System.Drawing.Color.Empty
        Me.TabControl1.ColorScheme.TabItemSelectedText = System.Drawing.Color.White
        Me.TabControl1.ColorScheme.TabItemText = System.Drawing.Color.White
        Me.TabControl1.ColorScheme.TabPanelBackground = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TabControl1.ColorScheme.TabPanelBackground2 = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TabControl1.ColorScheme.TabPanelBorder = System.Drawing.Color.Black
        Me.TabControl1.Controls.Add(Me.TabControlPanel18)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.FixedTabSize = New System.Drawing.Size(125, 40)
        Me.TabControl1.Location = New System.Drawing.Point(1, 1)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedTabFont = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.SelectedTabIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(658, 318)
        Me.TabControl1.Style = DevComponents.DotNetBar.eTabStripStyle.SimulatedTheme
        Me.TabControl1.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.TabControl1.TabIndex = 0
        Me.TabControl1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.TabControl1.Tabs.Add(Me.TabItem9)
        Me.TabControl1.Text = "TabControl1"
        '
        'TabControlPanel18
        '
        Me.TabControlPanel18.Controls.Add(Me.Panel3)
        Me.TabControlPanel18.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel18.Location = New System.Drawing.Point(128, 0)
        Me.TabControlPanel18.Name = "TabControlPanel18"
        Me.TabControlPanel18.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel18.Size = New System.Drawing.Size(530, 318)
        Me.TabControlPanel18.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TabControlPanel18.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TabControlPanel18.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel18.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel18.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel18.TabIndex = 3
        Me.TabControlPanel18.TabItem = Me.TabItem9
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Panel3.Controls.Add(Me.Panel6)
        Me.Panel3.Controls.Add(Me.Panel5)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(1, 1)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(528, 316)
        Me.Panel3.TabIndex = 2
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Panel6.Controls.Add(Me.Label21)
        Me.Panel6.Controls.Add(Me.Label22)
        Me.Panel6.Controls.Add(Me.PictureBox2)
        Me.Panel6.Controls.Add(Me.Label12)
        Me.Panel6.Controls.Add(Me.Label11)
        Me.Panel6.Controls.Add(Me.Label10)
        Me.Panel6.Controls.Add(Me.Label9)
        Me.Panel6.Controls.Add(Me.Label8)
        Me.Panel6.Controls.Add(Me.Label7)
        Me.Panel6.Location = New System.Drawing.Point(14, 97)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(504, 232)
        Me.Panel6.TabIndex = 2
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.BackColor = System.Drawing.Color.Transparent
        Me.Label21.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(15, 193)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(208, 17)
        Me.Label21.TabIndex = 17
        Me.Label21.Text = "Website: http://galvezsergio.url.ph"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.White
        Me.Label22.Location = New System.Drawing.Point(13, 59)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(208, 17)
        Me.Label22.TabIndex = 16
        Me.Label22.Text = "Website: http://galvezsergio.url.ph"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(347, 13)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(149, 160)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 3
        Me.PictureBox2.TabStop = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(13, 184)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(0, 15)
        Me.Label12.TabIndex = 5
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(22, 96)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(282, 85)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "- Microsoft Visual Studio 2010 (VB.NET)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "- Microsoft Access 2010 (Database / ADO." & _
            "NET)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "- SAP Crystal Report V. 13" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "- Cloud Toolkit N6" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "- DotnetBar2"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(11, 79)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(271, 17)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "The software uses the following components:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(11, 25)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(229, 17)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Programmed by: Galvez, Sergio Jr. M."
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(11, 42)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(334, 17)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Copyright © 2014 GALVEZ  Software. All rights reserved"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(11, 8)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(151, 17)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Program Version: 1.0.0.0"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Panel5.Controls.Add(Me.Label5)
        Me.Panel5.Controls.Add(Me.Label6)
        Me.Panel5.Controls.Add(Me.PictureBox1)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(528, 85)
        Me.Panel5.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("JACKIE", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(196, 14)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(294, 52)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Library System"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(93, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(118, 47)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "About "
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(22, 2)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(87, 80)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'TabItem9
        '
        Me.TabItem9.AttachedControl = Me.TabControlPanel18
        Me.TabItem9.Name = "TabItem9"
        Me.TabItem9.Text = "About"
        '
        'MainTab1About
        '
        Me.MainTab1About.AttachedControl = Me.TabControlPanel14
        Me.MainTab1About.Name = "MainTab1About"
        Me.MainTab1About.Text = "About"
        Me.MainTab1About.Visible = False
        '
        'TabControlPanel6
        '
        Me.TabControlPanel6.Controls.Add(Me.SplitContainer8)
        Me.TabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel6.Location = New System.Drawing.Point(0, 0)
        Me.TabControlPanel6.Name = "TabControlPanel6"
        Me.TabControlPanel6.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel6.Size = New System.Drawing.Size(660, 320)
        Me.TabControlPanel6.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel6.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel6.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Top), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel6.Style.GradientAngle = -90
        Me.TabControlPanel6.TabIndex = 2
        Me.TabControlPanel6.TabItem = Me.MainTab1Users
        '
        'SplitContainer8
        '
        Me.SplitContainer8.BackColor = System.Drawing.Color.Black
        Me.SplitContainer8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer8.Location = New System.Drawing.Point(1, 1)
        Me.SplitContainer8.Name = "SplitContainer8"
        '
        'SplitContainer8.Panel1
        '
        Me.SplitContainer8.Panel1.Controls.Add(Me.dtgMemRes)
        Me.SplitContainer8.Panel1.Controls.Add(Me.ToolStrip6)
        Me.SplitContainer8.Panel1.Controls.Add(Me.ToolStrip3)
        '
        'SplitContainer8.Panel2
        '
        Me.SplitContainer8.Panel2.AutoScroll = True
        Me.SplitContainer8.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SplitContainer8.Panel2MinSize = 317
        Me.SplitContainer8.Size = New System.Drawing.Size(658, 318)
        Me.SplitContainer8.SplitterDistance = 336
        Me.SplitContainer8.TabIndex = 7
        '
        'dtgMemRes
        '
        Me.dtgMemRes.AllowUserToAddRows = False
        Me.dtgMemRes.AllowUserToDeleteRows = False
        DataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.White
        Me.dtgMemRes.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle14
        Me.dtgMemRes.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dtgMemRes.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgMemRes.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.dtgMemRes.ColumnHeadersHeight = 24
        Me.dtgMemRes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dtgMemRes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn14, Me.Column5})
        Me.dtgMemRes.ContextMenuStrip = Me.ContextMember
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgMemRes.DefaultCellStyle = DataGridViewCellStyle16
        Me.dtgMemRes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtgMemRes.EnableHeadersVisualStyles = False
        Me.dtgMemRes.GridColor = System.Drawing.SystemColors.ControlDarkDark
        Me.dtgMemRes.Location = New System.Drawing.Point(0, 25)
        Me.dtgMemRes.MultiSelect = False
        Me.dtgMemRes.Name = "dtgMemRes"
        Me.dtgMemRes.ReadOnly = True
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgMemRes.RowHeadersDefaultCellStyle = DataGridViewCellStyle17
        Me.dtgMemRes.RowHeadersWidth = 35
        Me.dtgMemRes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgMemRes.Size = New System.Drawing.Size(334, 266)
        Me.dtgMemRes.TabIndex = 6
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "member_id"
        Me.DataGridViewTextBoxColumn11.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "mem_name"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Name"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 200
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "contact_no"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Contact #"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Width = 120
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "course_description"
        Me.DataGridViewTextBoxColumn14.HeaderText = "Course"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Width = 200
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "member_description"
        Me.Column5.HeaderText = "Type"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        '
        'ToolStrip6
        '
        Me.ToolStrip6.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolStrip6.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolStrip6.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolMemcount})
        Me.ToolStrip6.Location = New System.Drawing.Point(0, 291)
        Me.ToolStrip6.Name = "ToolStrip6"
        Me.ToolStrip6.Size = New System.Drawing.Size(334, 25)
        Me.ToolStrip6.TabIndex = 9
        Me.ToolStrip6.Text = "Book Title"
        '
        'ToolMemcount
        '
        Me.ToolMemcount.Name = "ToolMemcount"
        Me.ToolMemcount.Size = New System.Drawing.Size(13, 22)
        Me.ToolMemcount.Text = "0"
        '
        'ToolStrip3
        '
        Me.ToolStrip3.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolStrip3.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel4, Me.txtsearchmember})
        Me.ToolStrip3.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip3.Name = "ToolStrip3"
        Me.ToolStrip3.Size = New System.Drawing.Size(334, 25)
        Me.ToolStrip3.TabIndex = 7
        Me.ToolStrip3.Text = "Book Title"
        '
        'ToolStripLabel4
        '
        Me.ToolStripLabel4.Name = "ToolStripLabel4"
        Me.ToolStripLabel4.Size = New System.Drawing.Size(93, 22)
        Me.ToolStripLabel4.Text = "Member Name :"
        '
        'txtsearchmember
        '
        Me.txtsearchmember.Name = "txtsearchmember"
        Me.txtsearchmember.Size = New System.Drawing.Size(200, 25)
        '
        'MainTab1Users
        '
        Me.MainTab1Users.AttachedControl = Me.TabControlPanel6
        Me.MainTab1Users.Icon = CType(resources.GetObject("MainTab1Users.Icon"), System.Drawing.Icon)
        Me.MainTab1Users.Name = "MainTab1Users"
        Me.MainTab1Users.Text = "Library Members"
        Me.MainTab1Users.Visible = False
        '
        'TabControlPanel7
        '
        Me.TabControlPanel7.Controls.Add(Me.MainTab3)
        Me.TabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel7.Location = New System.Drawing.Point(0, 0)
        Me.TabControlPanel7.Name = "TabControlPanel7"
        Me.TabControlPanel7.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel7.Size = New System.Drawing.Size(660, 320)
        Me.TabControlPanel7.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel7.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel7.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Top), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel7.Style.GradientAngle = -90
        Me.TabControlPanel7.TabIndex = 3
        Me.TabControlPanel7.TabItem = Me.Maintab1Books
        '
        'MainTab3
        '
        Me.MainTab3.BackColor = System.Drawing.Color.White
        Me.MainTab3.CanReorderTabs = True
        Me.MainTab3.CloseButtonOnTabsAlwaysDisplayed = False
        Me.MainTab3.CloseButtonPosition = DevComponents.DotNetBar.eTabCloseButtonPosition.Right
        Me.MainTab3.ColorScheme.TabBackground = System.Drawing.SystemColors.ControlDarkDark
        Me.MainTab3.ColorScheme.TabBackground2 = System.Drawing.Color.Empty
        Me.MainTab3.ColorScheme.TabBorder = System.Drawing.Color.Black
        Me.MainTab3.ColorScheme.TabItemBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(215, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(249, Byte), Integer)), 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(199, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(248, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(179, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(245, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(215, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(247, Byte), Integer)), 1.0!)})
        Me.MainTab3.ColorScheme.TabItemHotBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(235, Byte), Integer)), 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(168, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(218, Byte), Integer), CType(CType(89, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(141, Byte), Integer)), 1.0!)})
        Me.MainTab3.ColorScheme.TabItemHotText = System.Drawing.Color.White
        Me.MainTab3.ColorScheme.TabItemSelectedBackground = System.Drawing.SystemColors.Highlight
        Me.MainTab3.ColorScheme.TabItemSelectedBackgroundColorBlend.AddRange(New DevComponents.DotNetBar.BackgroundColorBlend() {New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.White, 0.0!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 0.45!), New DevComponents.DotNetBar.BackgroundColorBlend(System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer)), 1.0!)})
        Me.MainTab3.ColorScheme.TabItemSelectedBorder = System.Drawing.Color.Empty
        Me.MainTab3.ColorScheme.TabItemSelectedBorderLight = System.Drawing.Color.Empty
        Me.MainTab3.ColorScheme.TabItemSelectedText = System.Drawing.Color.White
        Me.MainTab3.ColorScheme.TabItemText = System.Drawing.Color.White
        Me.MainTab3.ColorScheme.TabPanelBackground = System.Drawing.Color.White
        Me.MainTab3.ColorScheme.TabPanelBackground2 = System.Drawing.Color.Empty
        Me.MainTab3.ColorScheme.TabPanelBorder = System.Drawing.Color.Black
        Me.MainTab3.Controls.Add(Me.TabControlPanel9)
        Me.MainTab3.Controls.Add(Me.TabControlPanel10)
        Me.MainTab3.Controls.Add(Me.TabControlPanel11)
        Me.MainTab3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainTab3.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainTab3.ForeColor = System.Drawing.Color.White
        Me.MainTab3.Location = New System.Drawing.Point(1, 1)
        Me.MainTab3.Name = "MainTab3"
        Me.MainTab3.SelectedTabFont = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainTab3.SelectedTabIndex = 0
        Me.MainTab3.Size = New System.Drawing.Size(658, 318)
        Me.MainTab3.Style = DevComponents.DotNetBar.eTabStripStyle.Flat
        Me.MainTab3.TabIndex = 3
        Me.MainTab3.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.MainTab3.Tabs.Add(Me.MainTab3Book)
        Me.MainTab3.Tabs.Add(Me.MainTab3Authors)
        Me.MainTab3.Tabs.Add(Me.MainTab3Vendor)
        Me.MainTab3.Text = "TabControl2"
        '
        'TabControlPanel10
        '
        Me.TabControlPanel10.Controls.Add(Me.SplitContainer3)
        Me.TabControlPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel10.Location = New System.Drawing.Point(0, 27)
        Me.TabControlPanel10.Name = "TabControlPanel10"
        Me.TabControlPanel10.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel10.Size = New System.Drawing.Size(658, 291)
        Me.TabControlPanel10.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel10.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel10.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel10.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel10.Style.GradientAngle = 90
        Me.TabControlPanel10.TabIndex = 4
        Me.TabControlPanel10.TabItem = Me.MainTab3Vendor
        '
        'SplitContainer3
        '
        Me.SplitContainer3.BackColor = System.Drawing.Color.Black
        Me.SplitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer3.Location = New System.Drawing.Point(1, 1)
        Me.SplitContainer3.Name = "SplitContainer3"
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.dtgVendor)
        Me.SplitContainer3.Panel1.Controls.Add(Me.ToolStrip2)
        '
        'SplitContainer3.Panel2
        '
        Me.SplitContainer3.Panel2.AutoScroll = True
        Me.SplitContainer3.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SplitContainer3.Panel2MinSize = 317
        Me.SplitContainer3.Size = New System.Drawing.Size(656, 289)
        Me.SplitContainer3.SplitterDistance = 335
        Me.SplitContainer3.TabIndex = 8
        '
        'dtgVendor
        '
        Me.dtgVendor.AllowUserToAddRows = False
        Me.dtgVendor.AllowUserToDeleteRows = False
        DataGridViewCellStyle22.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle22.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle22.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.White
        Me.dtgVendor.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle22
        Me.dtgVendor.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dtgVendor.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle23.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle23.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle23.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgVendor.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle23
        Me.dtgVendor.ColumnHeadersHeight = 24
        Me.dtgVendor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dtgVendor.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn10, Me.vendor_contact_no})
        DataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle24.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle24.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle24.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle24.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgVendor.DefaultCellStyle = DataGridViewCellStyle24
        Me.dtgVendor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtgVendor.EnableHeadersVisualStyles = False
        Me.dtgVendor.GridColor = System.Drawing.SystemColors.ControlDarkDark
        Me.dtgVendor.Location = New System.Drawing.Point(0, 25)
        Me.dtgVendor.MultiSelect = False
        Me.dtgVendor.Name = "dtgVendor"
        Me.dtgVendor.ReadOnly = True
        DataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle25.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle25.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle25.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgVendor.RowHeadersDefaultCellStyle = DataGridViewCellStyle25
        Me.dtgVendor.RowHeadersWidth = 35
        Me.dtgVendor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgVendor.Size = New System.Drawing.Size(333, 262)
        Me.dtgVendor.TabIndex = 6
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "vendor_id"
        Me.DataGridViewTextBoxColumn8.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 30
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "vendor_name"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Vendor Name"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 150
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "vendor_address"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Address"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 200
        '
        'vendor_contact_no
        '
        Me.vendor_contact_no.DataPropertyName = "vendor_contact_no"
        Me.vendor_contact_no.HeaderText = "Contact #"
        Me.vendor_contact_no.Name = "vendor_contact_no"
        Me.vendor_contact_no.ReadOnly = True
        '
        'ToolStrip2
        '
        Me.ToolStrip2.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel2, Me.TxtsearchVendor})
        Me.ToolStrip2.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip2.Name = "ToolStrip2"
        Me.ToolStrip2.Size = New System.Drawing.Size(333, 25)
        Me.ToolStrip2.TabIndex = 7
        Me.ToolStrip2.Text = "Book Title"
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(86, 22)
        Me.ToolStripLabel2.Text = "Vendor Name :"
        '
        'TxtsearchVendor
        '
        Me.TxtsearchVendor.Name = "TxtsearchVendor"
        Me.TxtsearchVendor.Size = New System.Drawing.Size(200, 25)
        '
        'MainTab3Vendor
        '
        Me.MainTab3Vendor.AttachedControl = Me.TabControlPanel10
        Me.MainTab3Vendor.Icon = CType(resources.GetObject("MainTab3Vendor.Icon"), System.Drawing.Icon)
        Me.MainTab3Vendor.Name = "MainTab3Vendor"
        Me.MainTab3Vendor.Text = "Vendors"
        '
        'TabControlPanel11
        '
        Me.TabControlPanel11.Controls.Add(Me.SplitContainer2)
        Me.TabControlPanel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel11.Location = New System.Drawing.Point(0, 27)
        Me.TabControlPanel11.Name = "TabControlPanel11"
        Me.TabControlPanel11.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel11.Size = New System.Drawing.Size(658, 291)
        Me.TabControlPanel11.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel11.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel11.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel11.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel11.Style.GradientAngle = 90
        Me.TabControlPanel11.TabIndex = 2
        Me.TabControlPanel11.TabItem = Me.MainTab3Authors
        '
        'SplitContainer2
        '
        Me.SplitContainer2.BackColor = System.Drawing.Color.Black
        Me.SplitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(1, 1)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.dtgAuthor)
        Me.SplitContainer2.Panel1.Controls.Add(Me.ToolStrip1)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.AutoScroll = True
        Me.SplitContainer2.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SplitContainer2.Panel2MinSize = 317
        Me.SplitContainer2.Size = New System.Drawing.Size(656, 289)
        Me.SplitContainer2.SplitterDistance = 335
        Me.SplitContainer2.TabIndex = 7
        '
        'dtgAuthor
        '
        Me.dtgAuthor.AllowUserToAddRows = False
        Me.dtgAuthor.AllowUserToDeleteRows = False
        DataGridViewCellStyle26.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle26.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.White
        Me.dtgAuthor.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle26
        Me.dtgAuthor.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dtgAuthor.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle27.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle27.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle27.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle27.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgAuthor.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle27
        Me.dtgAuthor.ColumnHeadersHeight = 24
        Me.dtgAuthor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dtgAuthor.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7})
        DataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle28.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle28.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle28.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle28.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgAuthor.DefaultCellStyle = DataGridViewCellStyle28
        Me.dtgAuthor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtgAuthor.EnableHeadersVisualStyles = False
        Me.dtgAuthor.GridColor = System.Drawing.SystemColors.ControlDarkDark
        Me.dtgAuthor.Location = New System.Drawing.Point(0, 25)
        Me.dtgAuthor.MultiSelect = False
        Me.dtgAuthor.Name = "dtgAuthor"
        Me.dtgAuthor.ReadOnly = True
        DataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle29.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle29.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle29.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgAuthor.RowHeadersDefaultCellStyle = DataGridViewCellStyle29
        Me.dtgAuthor.RowHeadersWidth = 35
        Me.dtgAuthor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgAuthor.Size = New System.Drawing.Size(333, 262)
        Me.dtgAuthor.TabIndex = 6
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "author_id"
        Me.DataGridViewTextBoxColumn5.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 30
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "firstname"
        Me.DataGridViewTextBoxColumn6.HeaderText = "First Name"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 150
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "lastname"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Last Name"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 150
        '
        'ToolStrip1
        '
        Me.ToolStrip1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel1, Me.TxtsearchAuthor})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(333, 25)
        Me.ToolStrip1.TabIndex = 7
        Me.ToolStrip1.Text = "Book Title"
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(85, 22)
        Me.ToolStripLabel1.Text = "Author Name :"
        '
        'TxtsearchAuthor
        '
        Me.TxtsearchAuthor.Name = "TxtsearchAuthor"
        Me.TxtsearchAuthor.Size = New System.Drawing.Size(200, 25)
        '
        'MainTab3Authors
        '
        Me.MainTab3Authors.AttachedControl = Me.TabControlPanel11
        Me.MainTab3Authors.Icon = CType(resources.GetObject("MainTab3Authors.Icon"), System.Drawing.Icon)
        Me.MainTab3Authors.Name = "MainTab3Authors"
        Me.MainTab3Authors.Text = "Authors"
        '
        'TabControlPanel9
        '
        Me.TabControlPanel9.Controls.Add(Me.SplitContainer1)
        Me.TabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel9.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControlPanel9.Location = New System.Drawing.Point(0, 27)
        Me.TabControlPanel9.Name = "TabControlPanel9"
        Me.TabControlPanel9.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel9.Size = New System.Drawing.Size(658, 291)
        Me.TabControlPanel9.Style.BackColor1.Color = System.Drawing.Color.White
        Me.TabControlPanel9.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel9.Style.BorderColor.Color = System.Drawing.Color.Black
        Me.TabControlPanel9.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel9.Style.GradientAngle = 90
        Me.TabControlPanel9.TabIndex = 1
        Me.TabControlPanel9.TabItem = Me.MainTab3Book
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BackColor = System.Drawing.Color.Black
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(1, 1)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.dtgBook)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ToolStrip5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ToolStrip4)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.AutoScroll = True
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SplitContainer1.Panel2MinSize = 317
        Me.SplitContainer1.Size = New System.Drawing.Size(656, 289)
        Me.SplitContainer1.SplitterDistance = 335
        Me.SplitContainer1.TabIndex = 6
        '
        'dtgBook
        '
        Me.dtgBook.AllowUserToAddRows = False
        Me.dtgBook.AllowUserToDeleteRows = False
        DataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle18.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.White
        Me.dtgBook.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle18
        Me.dtgBook.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dtgBook.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle19.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle19.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgBook.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle19
        Me.dtgBook.ColumnHeadersHeight = 24
        Me.dtgBook.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dtgBook.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column4, Me.Column1, Me.Column2, Me.Column3})
        Me.dtgBook.ContextMenuStrip = Me.ContextBook
        DataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle20.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle20.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle20.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dtgBook.DefaultCellStyle = DataGridViewCellStyle20
        Me.dtgBook.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dtgBook.EnableHeadersVisualStyles = False
        Me.dtgBook.GridColor = System.Drawing.SystemColors.ControlDarkDark
        Me.dtgBook.Location = New System.Drawing.Point(0, 25)
        Me.dtgBook.MultiSelect = False
        Me.dtgBook.Name = "dtgBook"
        Me.dtgBook.ReadOnly = True
        DataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle21.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        DataGridViewCellStyle21.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle21.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dtgBook.RowHeadersDefaultCellStyle = DataGridViewCellStyle21
        Me.dtgBook.RowHeadersWidth = 35
        Me.dtgBook.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dtgBook.Size = New System.Drawing.Size(333, 237)
        Me.dtgBook.TabIndex = 6
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "BookID"
        Me.Column4.HeaderText = "ID"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Visible = False
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "title"
        Me.Column1.HeaderText = "Title"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Width = 280
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "category_description"
        Me.Column2.HeaderText = "Category"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 120
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "author_name"
        Me.Column3.HeaderText = "Author"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 200
        '
        'ToolStrip5
        '
        Me.ToolStrip5.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolStrip5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolStrip5.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolBookscount})
        Me.ToolStrip5.Location = New System.Drawing.Point(0, 262)
        Me.ToolStrip5.Name = "ToolStrip5"
        Me.ToolStrip5.Size = New System.Drawing.Size(333, 25)
        Me.ToolStrip5.TabIndex = 8
        Me.ToolStrip5.Text = "Book Title"
        '
        'ToolBookscount
        '
        Me.ToolBookscount.Name = "ToolBookscount"
        Me.ToolBookscount.Size = New System.Drawing.Size(13, 22)
        Me.ToolBookscount.Text = "0"
        '
        'ToolStrip4
        '
        Me.ToolStrip4.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ToolStrip4.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel3, Me.txtBookSearch})
        Me.ToolStrip4.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip4.Name = "ToolStrip4"
        Me.ToolStrip4.Size = New System.Drawing.Size(333, 25)
        Me.ToolStrip4.TabIndex = 7
        Me.ToolStrip4.Text = "Book Title"
        '
        'ToolStripLabel3
        '
        Me.ToolStripLabel3.Name = "ToolStripLabel3"
        Me.ToolStripLabel3.Size = New System.Drawing.Size(66, 22)
        Me.ToolStripLabel3.Text = "Book Title :"
        '
        'txtBookSearch
        '
        Me.txtBookSearch.Name = "txtBookSearch"
        Me.txtBookSearch.Size = New System.Drawing.Size(200, 25)
        '
        'MainTab3Book
        '
        Me.MainTab3Book.AttachedControl = Me.TabControlPanel9
        Me.MainTab3Book.Icon = CType(resources.GetObject("MainTab3Book.Icon"), System.Drawing.Icon)
        Me.MainTab3Book.Name = "MainTab3Book"
        Me.MainTab3Book.Text = "Registered Books"
        '
        'Maintab1Books
        '
        Me.Maintab1Books.AttachedControl = Me.TabControlPanel7
        Me.Maintab1Books.Icon = CType(resources.GetObject("Maintab1Books.Icon"), System.Drawing.Icon)
        Me.Maintab1Books.Name = "Maintab1Books"
        Me.Maintab1Books.Text = "Books"
        Me.Maintab1Books.Visible = False
        '
        'ToolStripContainer3
        '
        '
        'ToolStripContainer3.ContentPanel
        '
        Me.ToolStripContainer3.ContentPanel.Controls.Add(Me.MainTab1)
        Me.ToolStripContainer3.ContentPanel.Size = New System.Drawing.Size(660, 347)
        Me.ToolStripContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ToolStripContainer3.Location = New System.Drawing.Point(0, 25)
        Me.ToolStripContainer3.Name = "ToolStripContainer3"
        Me.ToolStripContainer3.Size = New System.Drawing.Size(660, 372)
        Me.ToolStripContainer3.TabIndex = 4
        Me.ToolStripContainer3.Text = "ToolStripContainer3"
        '
        'Main_Library
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(660, 397)
        Me.Controls.Add(Me.ToolStripContainer3)
        Me.Controls.Add(Me.ToolStripContainer2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.Name = "Main_Library"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Library System"
        Me.ToolStripContainer2.ContentPanel.ResumeLayout(False)
        Me.ToolStripContainer2.ContentPanel.PerformLayout()
        Me.ToolStripContainer2.ResumeLayout(False)
        Me.ToolStripContainer2.PerformLayout()
        Me.menuStrip.ResumeLayout(False)
        Me.menuStrip.PerformLayout()
        Me.ContextBorrow.ResumeLayout(False)
        Me.TabControlPanel3.ResumeLayout(False)
        Me.SplitContainer5.Panel1.ResumeLayout(False)
        Me.SplitContainer5.Panel1.PerformLayout()
        CType(Me.SplitContainer5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer5.ResumeLayout(False)
        CType(Me.dtgReturnRes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel8.ResumeLayout(False)
        CType(Me.MainTab4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MainTab4.ResumeLayout(False)
        Me.TabControlPanel13.ResumeLayout(False)
        Me.SplitContainer6.Panel1.ResumeLayout(False)
        Me.SplitContainer6.Panel1.PerformLayout()
        Me.SplitContainer6.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer6.ResumeLayout(False)
        CType(Me.Chart1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel15.ResumeLayout(False)
        Me.SplitContainer7.Panel1.ResumeLayout(False)
        Me.SplitContainer7.Panel1.PerformLayout()
        Me.SplitContainer7.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer7.ResumeLayout(False)
        Me.ContextMember.ResumeLayout(False)
        Me.ContextBook.ResumeLayout(False)
        CType(Me.MainTab1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MainTab1.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        CType(Me.MainTab2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MainTab2.ResumeLayout(False)
        Me.TabControlPanel5.ResumeLayout(False)
        CType(Me.dtgDueRes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel2.ResumeLayout(False)
        Me.splitborrow.Panel1.ResumeLayout(False)
        CType(Me.splitborrow, System.ComponentModel.ISupportInitialize).EndInit()
        Me.splitborrow.ResumeLayout(False)
        CType(Me.dtgBarrowRes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel4.ResumeLayout(False)
        Me.SplitContainer4.Panel1.ResumeLayout(False)
        CType(Me.SplitContainer4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer4.ResumeLayout(False)
        CType(Me.dtgLabRes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel14.ResumeLayout(False)
        CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabControlPanel18.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel6.ResumeLayout(False)
        Me.SplitContainer8.Panel1.ResumeLayout(False)
        Me.SplitContainer8.Panel1.PerformLayout()
        CType(Me.SplitContainer8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer8.ResumeLayout(False)
        CType(Me.dtgMemRes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip6.ResumeLayout(False)
        Me.ToolStrip6.PerformLayout()
        Me.ToolStrip3.ResumeLayout(False)
        Me.ToolStrip3.PerformLayout()
        Me.TabControlPanel7.ResumeLayout(False)
        CType(Me.MainTab3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MainTab3.ResumeLayout(False)
        Me.TabControlPanel10.ResumeLayout(False)
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel1.PerformLayout()
        CType(Me.SplitContainer3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer3.ResumeLayout(False)
        CType(Me.dtgVendor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip2.ResumeLayout(False)
        Me.ToolStrip2.PerformLayout()
        Me.TabControlPanel11.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.PerformLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        CType(Me.dtgAuthor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.TabControlPanel9.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.dtgBook, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip5.ResumeLayout(False)
        Me.ToolStrip5.PerformLayout()
        Me.ToolStrip4.ResumeLayout(False)
        Me.ToolStrip4.PerformLayout()
        Me.ToolStripContainer3.ContentPanel.ResumeLayout(False)
        Me.ToolStripContainer3.ResumeLayout(False)
        Me.ToolStripContainer3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ToolStripContainer2 As System.Windows.Forms.ToolStripContainer
    Private WithEvents menuStrip As System.Windows.Forms.MenuStrip
    Private WithEvents viewMenu As System.Windows.Forms.ToolStripMenuItem
    Private WithEvents ToolAbout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents SplitContainer5 As System.Windows.Forms.SplitContainer
    Friend WithEvents dtgReturnRes As System.Windows.Forms.DataGridView
    Friend WithEvents Column17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ToolStripret As System.Windows.Forms.ToolStrip
    Friend WithEvents TabControlPanel8 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents RecordsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolBooks As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolLibUsers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolReports As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolManage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextBook As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents toolbookdisp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolbookhide As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextBorrow As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents toolborrowdisp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolborrowhide As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolbookrefresh As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MainTab4 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel15 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem7 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel13 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents miniToolStrip As System.Windows.Forms.ToolStrip
    Friend WithEvents SplitContainer6 As System.Windows.Forms.SplitContainer
    Friend WithEvents dtpTodate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboReport As System.Windows.Forms.ComboBox
    Friend WithEvents btnLoad As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Chart1 As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents SplitContainer7 As System.Windows.Forms.SplitContainer
    Friend WithEvents BtnPreviewRpt As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Cmbreport As System.Windows.Forms.ComboBox
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents ContextMember As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ToolMemberRefresh As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolMemberDisp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolMemberHide As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolLabUser As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents toolborrowrefresh As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SuperTabItem3 As DevComponents.DotNetBar.TabItem
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents SuperTabItem4 As DevComponents.DotNetBar.TabItem
    Friend WithEvents SuperTabItem5 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabItem3 As DevComponents.DotNetBar.TabItem
    Friend WithEvents MainTab1Reports As DevComponents.DotNetBar.TabItem
    Friend WithEvents BottomToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents TopToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents RightToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents LeftToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents ContentPanel As System.Windows.Forms.ToolStripContentPanel
    Friend WithEvents MainTab1 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents MainTab2 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents splitborrow As System.Windows.Forms.SplitContainer
    Friend WithEvents dtgBarrowRes As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents SplitContainer4 As System.Windows.Forms.SplitContainer
    Friend WithEvents dtgLabRes As System.Windows.Forms.DataGridView
    Friend WithEvents Column21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TabItem4 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel5 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents dtgDueRes As System.Windows.Forms.DataGridView
    Friend WithEvents Column11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TabItem5 As DevComponents.DotNetBar.TabItem
    Friend WithEvents MainTab1Manage As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel7 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents MainTab3 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel9 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents dtgBook As System.Windows.Forms.DataGridView
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ToolStrip4 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripLabel3 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents txtBookSearch As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents MainTab3Book As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel10 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
    Friend WithEvents dtgVendor As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents vendor_contact_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ToolStrip2 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripLabel2 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents TxtsearchVendor As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents MainTab3Vendor As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel11 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents dtgAuthor As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents TxtsearchAuthor As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents MainTab3Authors As DevComponents.DotNetBar.TabItem
    Friend WithEvents Maintab1Books As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel14 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabControl1 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel18 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents TabItem9 As DevComponents.DotNetBar.TabItem
    Friend WithEvents MainTab1About As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel6 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents SplitContainer8 As System.Windows.Forms.SplitContainer
    Friend WithEvents dtgMemRes As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStrip3 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripLabel4 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents txtsearchmember As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents MainTab1Users As DevComponents.DotNetBar.TabItem
    Friend WithEvents ToolStripContainer3 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ToolStrip5 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolBookscount As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStrip6 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolMemcount As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolAccount As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    ''Friend WithEvents ListOfBooks1 As Login.ListOfBooks
End Class
